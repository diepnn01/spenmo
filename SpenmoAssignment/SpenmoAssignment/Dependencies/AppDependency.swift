//
//  AppDependency.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation
let SPAppDependency = AppDependency.shared
class AppDependency {
    static let shared: AppDependency = AppDependency()
    lazy var configation: AppConfiguration = {
       return AppConfiguration()
    }()
    
    lazy var httpManager: HTTPManagerProtocol = {
       return HTTPManager(configuration: configation)
    }()
    
    lazy var filterApi: FilterApi = {
       return FilterApiImpl(networkApi: httpManager)
    }()
    
    lazy var filterRepository: FilterRepository = {
       return FilterRepositoryImpl(filterApi: filterApi)
    }()
    
    lazy var filterStorage: FilterStorage = {
       return FilterStorageImpl(filterRepository: filterRepository)
    }()
}
