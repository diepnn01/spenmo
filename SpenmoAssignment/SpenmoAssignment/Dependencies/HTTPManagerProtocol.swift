//
//  HTTPProtocolManager.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/28.
//

import Foundation

protocol HTTPManagerProtocol {
    func get(url: String, params: [String: String], completionBlock: @escaping (AppApiResponse) -> Void)
}

class HTTPManager: HTTPManagerProtocol {
    var configuration: AppConfiguration!
    init(configuration: AppConfiguration) {
        self.configuration = configuration
    }
    public func get(url: String, params: [String: String], completionBlock: @escaping (AppApiResponse) -> Void) {
        let endpoint = configuration.baseUrl.appendingPathComponent(url)

        var urlComponents = URLComponents(url: endpoint, resolvingAgainstBaseURL: true)!
        if !params.isEmpty {
            urlComponents.queryItems = params.compactMap({URLQueryItem(name: $0.key, value: $0.value)})
        }
        var request = URLRequest(url: urlComponents.url!)
        request.httpMethod = "GET"
        let task = URLSession.shared.dataTask(with: request) { data, response, error in
            if let data = data,
               let json = try? JSONSerialization.jsonObject(with: data) as? [String: Any],
               let jsonData = try? JSONSerialization.data(withJSONObject: json["payload"] ?? [:])
            {
                completionBlock(AppApiResponse(data: jsonData, error: nil, message: "Success"))
            } else if let data = data {
                completionBlock(AppApiResponse(data: nil, error: AppApiError(message: String(data: data, encoding: .utf8) ?? error?.localizedDescription ?? "", errorCode: String(describing: (error as? NSError)?.code)), message: "Success"))
            } else {
                completionBlock(AppApiResponse(data: nil, error: AppApiError(message: error?.localizedDescription ?? "Something when wrong", errorCode: String(describing: (error as? NSError)?.code)), message: "Success"))
            }
        }
        task.resume()
    }
}
