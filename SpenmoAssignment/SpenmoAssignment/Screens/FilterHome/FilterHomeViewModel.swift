//
//  FilterHomeViewModel.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/28.
//

import Foundation

final class FilterHomeViewModel {
    private var filterStorage: FilterStorage
    private(set) var cardNumbers: [CardNumber] = []
    private(set) var cardHolders: [CardHolder] = []
    private(set) var teams: [Team] = []
    
    private(set) var cardNumbersSelected: [CardNumber] = []
    private(set) var cardHoldersSelected: [CardHolder] = []
    private(set) var teamsSelected: [Team] = []
    private(set) var cardStatusSelected = [CardStatus]()
    private(set) var cardTypeSelected = [CardType]()
    
    init(coordinator: RootCoordinator?, filterStorage: FilterStorage) {
        self.filterStorage = filterStorage
    }
    
    
    func onSetSelectedCardNumber(cards: [CardNumber]) {
        self.cardNumbersSelected = cards
    }
    
    func onSetSelectedCardHolder(cards: [CardHolder]) {
        self.cardHoldersSelected = cards
    }
    
    func onSetSelectedCardTeam(cards: [Team]) {
        self.teamsSelected = cards
    }
    
    func onSetSelectedCardStatus(cardStatuses: [CardStatus]) {
        self.cardStatusSelected = cardStatuses
    }
    
    func onSetSelectedCardType(cardTypes: [CardType]) {
        self.cardTypeSelected = cardTypes
    }
    
    func remove(item: Filterable, with filterType: FilterEnum) {
        switch filterType {
        case .cardNumber:
            self.cardNumbersSelected = self.cardNumbersSelected.filter({!($0.id == item.id && $0.name == item.name)})
        case .cardHolder:
            self.cardHoldersSelected = self.cardHoldersSelected.filter({!($0.id == item.id && $0.name == item.name)})
        case .team:
            self.teamsSelected = self.teamsSelected.filter({!($0.id == item.id && $0.name == item.name)})
        case .cardStatus:
            self.cardStatusSelected = self.cardStatusSelected.filter({!($0.id == item.id && $0.name == item.name)})
        case .cardType:
            self.cardTypeSelected = self.cardTypeSelected.filter({!($0.id == item.id && $0.name == item.name)})
        }
    }
    
    func clearAllSelectedItems() {
        cardTypeSelected.removeAll()
        cardStatusSelected.removeAll()
        cardHoldersSelected.removeAll()
        cardNumbersSelected.removeAll()
        teamsSelected.removeAll()
    }
    
    func prepareFilterResultJson(completed: ((String) -> Void)) {
        var dict = [String: Any]()
        if cardNumbersSelected.count > 0 {
            dict["cardNumbers"] = cardNumbersSelected.map({$0.toJson()})
        }
        
        if cardHoldersSelected.count > 0 {
            dict["cardHolders"] = cardHoldersSelected.map({$0.toJson()})
        }
        
        if teamsSelected.count > 0 {
            dict["teams"] = teamsSelected.map({$0.toJson()})
        }
        
        if cardStatusSelected.count > 0 {
            dict["cardStatus"] = cardStatusSelected.map({$0.toJson()})
        }
        
        if cardTypeSelected.count > 0 {
            dict["cardTypes"] = cardTypeSelected.map({$0.toJson()})
        }
        
        let checkValidDict = JSONSerialization.isValidJSONObject(dict)
        guard checkValidDict else {
            completed("Invalid data")
            return
        }
        do {
            let jsonData = try JSONSerialization.data(withJSONObject: dict, options: [])
            let decoded = try JSONSerialization.jsonObject(with: jsonData, options: [])
            completed("\(decoded)")
        } catch let e {
            completed("\(e.localizedDescription)")
        }
    }
}
