//
//  ViewController.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/28.
//

import UIKit
import RxSwift

final class FilterHomeController: BaseController {

    var disposeBag = DisposeBag()
    private var coordinator: ProjectCoordinator?
    private var viewModel: FilterHomeViewModel
    
    private lazy var clearAllButton: UIBarButtonItem = {
        let btn = UIBarButtonItem()
        btn.title = "FilterHomeController.ClearAll".localized
        return btn
    }()
    
    private lazy var contentView: FilterHomeView = {
        let view = FilterHomeView()
        view.backgroundColor = .white
        view.viewModel = viewModel
        view.bind()
        return view
    }()
    
    init(coordinator: ProjectCoordinator, viewModel: FilterHomeViewModel) {
        self.coordinator = coordinator
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        super.loadView()
        self.view = contentView
        self.title = "FilterHomeController.Title".localized
        self.navigationItem.rightBarButtonItem = clearAllButton
        initRx()
    }
    
    //MARK: Private methods
    private func initRx() {
        contentView.onSelectCondition
            .asObservable()
            .subscribe { [weak self]filterType in
                guard let filterType = filterType.element else { return }
                DispatchQueue.main.async {
                    self?.selectFilterCondition(type: filterType)
                }
                
            }.disposed(by: disposeBag)
        
        clearAllButton.rx.tap
            .asObservable()
            .subscribe { [weak self]_ in
                self?.clearAllAction()
            }.disposed(by: disposeBag)
        
        contentView.filterResultButton.rx.tap
            .asObservable()
            .subscribe { [weak self]_ in
                self?.viewModel.prepareFilterResultJson(completed: { msg in
                    self?.showAlert(msg: msg)
                })
            }.disposed(by: disposeBag)
    }
    
    @objc private func clearAllAction() {
        viewModel.clearAllSelectedItems()
        contentView.reloadData()
    }
    
    private func selectFilterCondition(type: FilterEnum) {
        
        let filterConditionsViewModel = FilterConditionsViewModel(filterType: type, filterStorage: SPAppDependency.filterStorage)
        switch type {
        case .cardNumber:
            filterConditionsViewModel.selectedItems = viewModel.cardNumbersSelected
        case .cardHolder:
            filterConditionsViewModel.selectedItems = viewModel.cardHoldersSelected
        case .team:
            filterConditionsViewModel.selectedItems = viewModel.teamsSelected
        case .cardType:
            filterConditionsViewModel.selectedItems = viewModel.cardTypeSelected
        case .cardStatus:
            filterConditionsViewModel.selectedItems = viewModel.cardStatusSelected
        }

        let filterConditionsController = FilterConditionsController(viewModel: filterConditionsViewModel)
        filterConditionsController.didChangeFilter = { [weak self] filters in
            switch type {
            case .cardNumber:
                self?.viewModel.onSetSelectedCardNumber(cards: (filters as? [CardNumber]) ?? [])
            case .cardHolder:
                self?.viewModel.onSetSelectedCardHolder(cards: (filters as? [CardHolder]) ?? [])
            case .team:
                self?.viewModel.onSetSelectedCardTeam(cards: (filters as? [Team]) ?? [])
            case .cardType:
                self?.viewModel.onSetSelectedCardType(cardTypes: filters as? [CardType] ?? [])
            case .cardStatus:
                self?.viewModel.onSetSelectedCardStatus(cardStatuses: filters as? [CardStatus] ?? [])
            }
            self?.contentView.reloadRow(of: type)
        }
        filterConditionsController.modalPresentationStyle = .overFullScreen
        self.present(filterConditionsController, animated: true, completion: nil)
    }
    
    
    private func showAlert(msg: String) {
        let alert = UIAlertController(title: "FilterHomeController.Filter Result".localized, message: msg, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        present(alert, animated: true, completion: nil)
    }
}

