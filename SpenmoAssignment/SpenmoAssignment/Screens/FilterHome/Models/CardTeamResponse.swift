//
//  CardTeamResponse.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 31/07/2022.
//

import Foundation
class CardTeamResponse: Codable {
    var teams: [Team] = []
}
