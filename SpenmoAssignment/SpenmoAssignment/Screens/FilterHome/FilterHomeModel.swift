//
//  FilterModel.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/28.
//

import Foundation

struct FilterModel: Codable {
    
}

protocol Filterable {
    var name: String { get }
    var id: String { get }
    var subTitle: String {get}
    func toJson() -> [String: Any]?
}

struct CardNumber: Filterable, Codable, Equatable {
    
    var cardName: String
    var cardId: String
    var cardNumber: String

    var name: String {
        return cardName
    }
    var id: String {
        return cardId
    }
    var subTitle: String {
        return "**** \(cardNumber)"
    }
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    func toJson() -> [String : Any]? {
        var params = [String: Any]()
        params["cardName"] = cardName
        params["cardId"] = cardId
        params["cardNumber"] = cardNumber
        return params
    }
}

struct CardHolder: Filterable, Codable, Equatable {
    var cardHolderId: String
    var firstName: String
    var lastName: String
    var subTitle: String {
        return ""
    }
    var id: String {
        return cardHolderId
    }
    var name: String {
        return "\(firstName) \(lastName)"
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    func toJson() -> [String : Any]? {
        var params = [String: Any]()
        params["cardHolderId"] = cardHolderId
        params["firstName"] = firstName
        params["lastName"] = lastName
        return params
    }
}

struct Team: Filterable, Codable, Equatable {
    var cardHolderId: String
    var teamName: String
    var id: String {
        return cardHolderId
    }
    var name: String {
        return teamName
    }
    
    var subTitle: String {
        return ""
    }
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.id == rhs.id
    }
    
    func toJson() -> [String : Any]? {
        var params = [String: Any]()
        params["cardHolderId"] = cardHolderId
        params["teamName"] = teamName
        return params
    }
}

struct CardStatus: Filterable {
    var status: String
    
    var id: String {
        return ""
    }
    var name: String {
        return status
    }
    
    var subTitle: String {
        return ""
    }
    
    func toJson() -> [String : Any]? {
        var params = [String: Any]()
        params["status"] = status
        return params
    }
}
struct CardType: Filterable {
    var type: String
    var id: String {
        return ""
    }
    var name: String {
        return type
    }
    
    var subTitle: String {
        return ""
    }
    
    func toJson() -> [String : Any]? {
        var params = [String: Any]()
        params["type"] = type
        return params
    }
}
