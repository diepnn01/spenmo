//
//  FilterHomeView.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/28.
//

import UIKit
import SnapKit
import RxSwift

final class FilterHomeView: UIView, BaseViewType {
    var disposeBag = DisposeBag()
    var onSelectCondition = PublishSubject<FilterEnum>()
    var viewModel: FilterHomeViewModel!
    lazy var tableView: UITableView = {
        let tableView = UITableView()
        tableView.delegate = self
        tableView.dataSource = self
        tableView.separatorStyle = .none
        return tableView
    }()
    
    lazy var filterResultButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor(named: "color_filter_condition_bg")
        let titleAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.white, .font: UIFont.avenirBlack(fontSize: 16)]
        let titleString = NSMutableAttributedString(string: "Filter Results", attributes: titleAttributes)
        button.setAttributedTitle(titleString, for: .normal)
        button.layer.cornerRadius = 28
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func layout() {
        [tableView, filterResultButton].forEach(addSubview)
        
        tableView.snp.makeConstraints { make in
            make.top.leading.trailing.equalTo(safeAreaLayoutGuide)
            make.bottom.equalTo(filterResultButton.snp.top).offset(-16)
        }
        
        filterResultButton.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.centerX.equalToSuperview()
            make.height.equalTo(56)
            make.bottom.equalTo(safeAreaLayoutGuide.snp.bottom).offset(-16)
        }
        tableView.register(FilterHomeCell.self, forCellReuseIdentifier: FilterHomeCell.className)
    }
    
    func bind() {
        

    }
    
    func reloadRow(of type: FilterEnum) {
        if let index = FilterEnum.allCases.firstIndex(where: {$0 == type}) {
            tableView.reloadRows(at: [IndexPath(item: index, section: 0)], with: .none)
        }
    }
    
    func reloadData() {
        tableView.reloadData()
    }
}

extension FilterHomeView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return FilterEnum.allCases.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterHomeCell.className, for: indexPath) as? FilterHomeCell else {
            return UITableViewCell()
        }
        let enumItem = FilterEnum.allCases[indexPath.row]
        cell.filterType = enumItem
        cell.bind()

        switch enumItem {
        case .cardNumber:
            cell.fillData(filterConditions: viewModel.cardNumbersSelected)
        case .cardHolder:
            cell.fillData(filterConditions: viewModel.cardHoldersSelected)
        case .team:
            cell.fillData(filterConditions: viewModel.teamsSelected)
        case .cardType:
            cell.fillData(filterConditions: viewModel.cardTypeSelected)
        case .cardStatus:
            cell.fillData(filterConditions: viewModel.cardStatusSelected)
        }
        
        cell.onSelectCondition = { [weak self] in
            self?.onSelectCondition.onNext(enumItem)
        }
        
        cell.boxFilterConditionsView.onRemoveSelectedItem = { [weak self] removeItem in
            self?.viewModel.remove(item: removeItem, with: enumItem)
            self?.tableView.reloadData()
        }
        
        return cell
    }
}

extension FilterHomeView: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
}
