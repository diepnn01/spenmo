//
//  FilterEnum.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/30.
//

import Foundation

enum FilterEnum: Int, CaseIterable {
    case cardNumber = 0
    case cardHolder
    case team
    case cardType
    case cardStatus
    
    var title: String? {
        switch self {
        case .cardNumber: return "Card Number"
        case .cardHolder: return "Card Holder"
        case .team: return "Team"
        case .cardType: return "Card Type"
        case .cardStatus: return "Card Status"
        }
    }
    
    var searchBoxPlaceholder: String? {
        switch self {
        case .cardNumber: return "Search Card Number"
        case .cardHolder: return "Search Card Holder"
        case .team: return "Search Team"
        case .cardType: return ""
        case .cardStatus: return ""
        }
    }
    
    var filterPlaceHolder: String? {
        switch self {
        case .cardNumber: return "Filter by Card Number"
        case .cardHolder: return "Filter by Card Holder"
        case .team: return "Filter by Team"
        case .cardType: return "Filter by Card Type"
        case .cardStatus: return "Filter by Card Status"
        }
    }
}
