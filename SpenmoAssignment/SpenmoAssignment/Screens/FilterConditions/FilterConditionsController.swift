//
//  FilterConditionsController.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/29.
//

import Foundation
import UIKit
import RxSwift

final class FilterConditionsController: BaseController {

    var disposeBag = DisposeBag()
    private var coordinator: ProjectCoordinator?
    private var viewModel: FilterConditionsViewModel
    private var filterConditionView: FilterConditionsView?
    var didChangeFilter: (([Filterable]) -> Void)?
    init(viewModel: FilterConditionsViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        let view = FilterConditionsView()
        view.isEnableSearch = viewModel.isEnableSearch
        filterConditionView = view
        view.viewModel = viewModel
        view.bind()
        self.view = view
        
        bindRx()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        guard viewModel.filterType == .cardNumber || viewModel.filterType == .team || viewModel.filterType == .cardHolder else {
            viewModel.fetchData()
            return
        }
        bindViewModel()
        self.filterConditionView?.showLoading()
        viewModel.fetchData()
    }
    private func bindRx() {
        filterConditionView?.dismissButton.rx.controlEvent(.touchUpInside)
            .asObservable().subscribe { [weak self]_ in
                self?.didChangeFilter?(self?.viewModel.selectedItems ?? [])
                self?.dismiss(animated: true, completion: nil)
            }.disposed(by: disposeBag)
        filterConditionView?.errorView.retryButton.rx.tap.subscribe { [weak self] _ in
            self?.filterConditionView?.showLoading()
            self?.viewModel.fetchData()
        }.disposed(by: disposeBag)

    }
    private func bindViewModel() {
        viewModel.didGetData = { [weak self] data in
            DispatchQueue.main.async {
                self?.filterConditionView?.reloadData()
                self?.filterConditionView?.hidenLoading()
            }
        }
        
        viewModel.didGetDataError = { [weak self] message in
            DispatchQueue.main.async {
                self?.filterConditionView?.showError(message: message)
                self?.filterConditionView?.hidenLoading()
            }
        }
    }
}

