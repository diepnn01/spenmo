//
//  FilterConditionsViewModel.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/29.
//

import Foundation

final class FilterConditionsViewModel {
    var items: [Filterable] = []
    var selectedItems: [Filterable] = []
    var searchItems: [Filterable] = []
    
    var didGetData: (([Filterable]) -> Void)?
    var didGetDataError: ((String) -> Void)?
    var filterType: FilterEnum
    let filterStorage: FilterStorage
    
    init(filterType: FilterEnum, filterStorage: FilterStorage) {
        self.filterType = filterType
        self.filterStorage = filterStorage
    }
    
    var isEnableSearch: Bool {
        return filterType == .cardHolder || filterType == .cardNumber || filterType == .team
    }
    
    var isMultipleSelection: Bool {
        return filterType == .cardHolder || filterType == .cardNumber || filterType == .team
    }
    
    func fetchData() {
        switch filterType {
        case .cardNumber:
            getCardNumbers()
        case .cardHolder:
            getCardHolder()
        case .team:
            getCardTeam()
        case .cardType:
            items = [CardType(type: "Virtual"), CardType(type: "Physical")]
            searchItems = items
            didGetData?(searchItems)
        case .cardStatus:
            items = [CardStatus(status: "Enable"), CardStatus(status: "Disable")]
            searchItems = items
            didGetData?(searchItems)
        }
        
    }
    func onSelected(card: Filterable) {
        if isMultipleSelection {
            if let index = selectedItems.firstIndex(where: {$0.id == card.id}) {
                selectedItems.remove(at: index)
            } else {
                selectedItems.append(card)
            }
        } else {
            selectedItems.removeAll()
            selectedItems.append(card)
        }
        
    }
    
    func checkSelectedItem(item: Filterable) -> Bool {
        if let _ = selectedItems.firstIndex(where: {$0.id == item.id && $0.name == item.name}) {
            return true
        } else {
            return false
        }
    }
    
    func search(with searchText: String) {
        guard !searchText.isEmpty else { return }
        searchItems = items.filter({$0.name.uppercased().contains(searchText.uppercased())})
        didGetData?(searchItems)
    }
    
    //MARK: - Fetch data from server
    func getCardNumbers() {
        filterStorage.getCardMembers { cards, error in
            if let cards = cards {
                self.items = cards
                self.didGetData?(cards)
            } else {
                self.didGetDataError?(error?.message ?? "No data")
            }
        }
    }
    
    func getCardHolder() {
        filterStorage.getCardHolders { cards, error in
            if let cards = cards {
                self.items = cards
                self.didGetData?(cards)
            } else {
                self.didGetDataError?(error?.message ?? "No data")
            }
        }
    }
    
    func getCardTeam() {
        filterStorage.getCardTeams { teams, error in
            if let teams = teams {
                self.items = teams
                self.didGetData?(teams)
            } else {
                self.didGetDataError?(error?.message ?? "No data")
            }
        }
    }
}
