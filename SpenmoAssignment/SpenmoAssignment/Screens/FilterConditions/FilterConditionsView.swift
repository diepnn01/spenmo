//
//  FilterConditionsView.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/29.
//

import UIKit
import RxSwift
import SnapKit

final class FilterConditionsView: UIView, BaseViewType  {

    var disposeBag: DisposeBag = DisposeBag()
    var viewModel: FilterConditionsViewModel! {
        didSet {
            maxHeight = (viewModel.filterType == .cardHolder || viewModel.filterType == .cardNumber || viewModel.filterType == .team) ? UIScreen.main.bounds.height/4 : UIScreen.main.bounds.height/2
            
            
            self.labelTitle.text = viewModel.filterType.title
            self.searchBoxView.placeholder = viewModel.filterType.searchBoxPlaceholder
        }
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK: - UI components
    lazy var dismissButton: UIButton = {
        let gesture = UIButton()
        return gesture
    }()
    
    lazy private var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 24
        view.layer.maskedCorners = [.layerMaxXMinYCorner, .layerMinXMinYCorner]
        view.layer.masksToBounds = true
        return view
    }()

    lazy private var topLineView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.black.withAlphaComponent(0.1)
        view.layer.cornerRadius = 2.5
        return view
    }()
    
    lazy private var labelTitle: UILabel = {
        let labelTitle = UILabel()
        labelTitle.font = UIFont.avenirBlack(fontSize: 20)
        labelTitle.textColor = .black
        labelTitle.textAlignment = .center
        return labelTitle
    }()
    
    lazy private var searchBoxView: SearchBoxView = {
        let searchBox = SearchBoxView(shouldSetup: true)
        searchBox.clipsToBounds = true
        return searchBox
    }()
    
    lazy private var tableView: UITableView = {
        let tableView = UITableView()
        tableView.dataSource = self
        tableView.delegate = self
        tableView.keyboardDismissMode = .onDrag
        return tableView
    }()
    lazy var errorView: FilterErrorView = {
       let view = FilterErrorView()
        view.isHidden = true
        view.backgroundColor = .white
        return view
    }()
    
    lazy var loadingView: UIView = {
       let view = UIView()
        let indicator = UIActivityIndicatorView()
        view.addSubview(indicator)
        indicator.startAnimating()
        indicator.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
        view.backgroundColor = .white
        view.isHidden = true
        return view
    }()
    
    
    //MARK: public properties
    var isEnableSearch: Bool = true {
        didSet {
            searchBoxView.snp.updateConstraints { make in
                make.height.equalTo(isEnableSearch ? 48 : 0)
            }
            
            tableView.snp.updateConstraints { make in
                make.top.equalTo(searchBoxView.snp.bottom).offset(isEnableSearch ? 20 : 0)
            }
        }
    }
    
    var maxHeight: CGFloat = 0 {
        didSet {
            containerView.snp.updateConstraints { make in
                make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(maxHeight)
            }
        }
    }
    
    //MARK: public methods
    func layout() {
        backgroundColor = UIColor(named: "color_bg_60")
        [dismissButton, containerView].forEach(addSubview)
        
        [topLineView,
         labelTitle,
         searchBoxView,
         tableView,
         errorView,
         loadingView].forEach(containerView.addSubview)
        
        dismissButton.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        containerView.snp.makeConstraints { make in
            make.leading.trailing.equalToSuperview()
            make.bottom.equalToSuperview()
            make.top.equalTo(safeAreaLayoutGuide.snp.top).offset(UIScreen.main.bounds.height/2)
        }
        
        topLineView.snp.makeConstraints { make in
            make.top.equalToSuperview().offset(13)
            make.height.equalTo(5)
            make.width.equalTo(80)
            make.centerX.equalToSuperview()
        }
        
        labelTitle.snp.makeConstraints { make in
            make.top.equalTo(topLineView.snp.bottom).offset(45)
            make.centerX.equalToSuperview()
            make.leading.greaterThanOrEqualToSuperview().offset(16)
            make.trailing.greaterThanOrEqualToSuperview().offset(-16)
        }
        
        searchBoxView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.height.equalTo(48)
            make.top.equalTo(labelTitle.snp.bottom).offset(32)
        }
        
        tableView.snp.makeConstraints { make in
            make.leading.trailing.bottom.equalToSuperview()
            make.top.equalTo(searchBoxView.snp.bottom).offset(20)
        }
        errorView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        loadingView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        setupTableView()
    }
    
    func bind() {
    
        searchBoxView.textFieldSearch.rx.text
            .debounce(.milliseconds(300), scheduler: MainScheduler.instance)
            .distinctUntilChanged()
            .subscribe { textSearch in
                self.viewModel.search(with: textSearch ?? "")
            }.disposed(by: disposeBag)

    }
    
    private func setupTableView() {
        tableView.register(FilterCardHolderCell.self, forCellReuseIdentifier: FilterCardHolderCell.className)
        tableView.register(FilterCardNumberCell.self, forCellReuseIdentifier: FilterCardNumberCell.className)
        tableView.register(FilterCommonCell.self, forCellReuseIdentifier: FilterCommonCell.className)
    }
    
}

//MARK: - Binding data

extension FilterConditionsView {
    
   
    func reloadData() {
        self.tableView.reloadData()
        errorView.isHidden = true
    }
    
    func showError(message: String) {
        self.errorView.messageLabel.text = message
        errorView.isHidden = false
    }
    func showLoading() {
        
        loadingView.isHidden = false
    }
    func hidenLoading() {
        loadingView.isHidden = true
    }
}

//MARK: - UITableViewDataSource
extension FilterConditionsView: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.searchItems.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let card = viewModel.searchItems[indexPath.row]
        switch viewModel.filterType {
        case .cardNumber:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterCardNumberCell.className, for: indexPath) as? FilterCardNumberCell else {
                return UITableViewCell()
            }
            
            cell.cardNumber = card
            cell.isSelectedItem = viewModel.checkSelectedItem(item: card)
            return cell
        case .cardHolder:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterCardHolderCell.className, for: indexPath) as? FilterCardHolderCell else {
                return UITableViewCell()
            }
            cell.cardHolder = card
            cell.isSelectedItem = viewModel.checkSelectedItem(item: card)
            return cell
        case .team:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterCommonCell.className, for: indexPath) as? FilterCommonCell else {
                return UITableViewCell()
            }
            cell.name = card.name
            cell.selectedItem = viewModel.checkSelectedItem(item: card)
            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: FilterCommonCell.className, for: indexPath) as? FilterCommonCell else {
                return UITableViewCell()
            }
            cell.name = card.name
            cell.selectedItem = viewModel.checkSelectedItem(item: card)
            return cell
        }
    
    }
}

//MARK: - UITableViewDelegate
extension FilterConditionsView: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let card = viewModel.searchItems[indexPath.row]
        viewModel.onSelected(card: card)
        if viewModel.isMultipleSelection {
            tableView.reloadRows(at: [indexPath], with: .none)
        } else {
            tableView.reloadData()
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}
