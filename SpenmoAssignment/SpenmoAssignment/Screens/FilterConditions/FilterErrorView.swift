//
//  FilterErrorView.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import UIKit

class FilterErrorView: UIView {

    lazy var messageLabel: UILabel = {
        var label = UILabel()
        label.numberOfLines = 0
        label.textAlignment = .center
        return label
    }()
    
    lazy var retryButton: UIButton = {
        var button = UIButton()
        button.setTitle("Retry", for: .normal)
        button.setTitleColor(.blue, for: .normal)
        button.layer.cornerRadius = 8
        button.layer.borderColor = UIColor.blue.cgColor
        button.layer.borderWidth = 1
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func layout() {
        let stack = UIStackView(arrangedSubviews: [messageLabel, retryButton])
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .center
        stack.spacing = 10
        addSubview(stack)
        
        stack.snp.makeConstraints { make in
            make.center.equalToSuperview()
            make.left.right.equalToSuperview()
        }
        retryButton.snp.makeConstraints { make in
            make.size.equalTo(CGSize(width: 100, height: 48))
        }
    }
}
