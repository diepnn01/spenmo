//
//  AppConfiguration.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation
class AppConfiguration {
    var baseUrlString: String {
        return "https://spenmo-test.free.beeceptor.com"
    }
    var baseUrl: URL {
        return URL(string: "https://spenmo-test.free.beeceptor.com")!
    }
}
