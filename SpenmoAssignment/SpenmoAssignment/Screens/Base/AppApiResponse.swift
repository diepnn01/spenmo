//
//  AppApiResponse.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation
struct AppApiResponse {
    var data: Data?
    var error: AppApiError?
    var message: String?
    
    func parseData<T: Codable>(type: T.Type) -> T? {
        guard let data = data else {
            return nil
        }
        return try? JSONDecoder().decode(T.self, from: data)
    }
}

struct AppApiError {
    var message: String
    var errorCode: String
}
