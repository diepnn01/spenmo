//
//  FilterRepositoryImpl.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation
class FilterRepositoryImpl: FilterRepository {
    let filterApi: FilterApi!
    init(filterApi: FilterApi) {
        self.filterApi = filterApi
    }
    
    func getCardMembers(completed: ((CardNumberResponse?, AppApiError?) -> Void)?) {
        filterApi.getCardMembers { response in
            completed?(response.parseData(type: CardNumberResponse.self), response.error)
        }
    }
    
    func getCardHolders(completed: ((CardHolderResponse?, AppApiError?) -> Void)?) {
        filterApi.getCardHolders { response in
            completed?(response.parseData(type: CardHolderResponse.self), response.error)
        }
    }
    
    func getCardTeams(completed: ((CardTeamResponse?, AppApiError?) -> Void)?) {
        filterApi.getCardTeams { response in
            completed?(response.parseData(type: CardTeamResponse.self), response.error)
        }
    }
}
