//
//  FilterRepository.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation

protocol FilterRepository {
    func getCardMembers(completed: ((CardNumberResponse?, AppApiError?) -> Void)?)
    func getCardHolders(completed: ((CardHolderResponse?, AppApiError?) -> Void)?)
    func getCardTeams(completed: ((CardTeamResponse?, AppApiError?) -> Void)?)
}
