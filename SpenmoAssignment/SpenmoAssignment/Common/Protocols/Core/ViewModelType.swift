//
//  ViewModelType.swift
//  SEED
//
//  Created by Diep Nguyen on 17/4/19.
//  Copyright © 2019 Diep Nguyen. All rights reserved.
//

import RxCocoa

protocol ViewModelType: class { }

protocol TrackableViewModelType { }

typealias TransformableViewModelType = ViewModelType & Transformable
