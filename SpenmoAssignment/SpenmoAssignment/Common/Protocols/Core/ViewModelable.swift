//
//  ViewModelable.swift
//  SEED
//
//  Created by Diep Nguyen on 14/8/19.
//  Copyright © 2019 Diep Nguyen. All rights reserved.
//

import UIKit

/// Responsible for presentation and passing user events to `ViewModel` object.
protocol ViewModelable {
   associatedtype ViewModel

   /// `ViewModel` object used for transforming user input into output for presentation.
   var viewModel: ViewModel! { get set }
}

protocol Parameterisable {
   associatedtype Parameter
   var parameter: Parameter! { get set }
}

extension ViewModelable where Self: ViewType, Self: UIViewController,
   Self.ViewModel: ViewModelType, Self: Parameterisable {
   /// Entry point for `View` creation.
   ///
   /// - Parameter viewModel: `ViewModel` object used for binding.
   /// - Parameter parameter: `Parameter` optional object to pass in
   init(viewModel: ViewModel, parameter: Parameter) {
      self.init(nibName: nil, bundle: nil)
      self.parameter = parameter
      self.viewModel = viewModel
      self.setup()
   }
}

extension ViewModelable where Self: ViewType, Self: UIViewController, Self.ViewModel: ViewModelType {
   /// Entry point for `View` creation.
   ///
   /// - Parameter viewModel: `ViewModel` object used for binding.
   init(viewModel: ViewModel) {
      self.init(nibName: nil, bundle: nil)
      self.viewModel = viewModel
      self.setup()
   }
}

extension ViewModelable where Self: ViewType, Self: UIView, Self.ViewModel: ViewModelType {
   /// Entry point for `View` creation.
   ///
   /// - Parameter viewModel: `ViewModel` object used for binding.
   init(viewModel: ViewModel) {
      self.init()
      self.viewModel = viewModel
      self.setup()
   }
}
