//
//  Transformable.swift
//  SEED
//
//  Created by Diep Nguyen on 14/8/19.
//  Copyright © 2019 Diep Nguyen. All rights reserved.
//

/// Responsible for transforming input into output.
protocol Transformable {
   associatedtype Input
   associatedtype Output

   /// Transforms `Input` into `Output`.
   ///
   /// - Parameter input: Data to transform.
   /// - Returns: Transformed data.
   func transform(input: Input) -> Output
}
