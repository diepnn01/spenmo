//
//  FilterApiImpl.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation
class FilterApiImpl: FilterApi {
    private let cardMemberPath = "cards"
    private let cardHodlersPath = "cardholders"
    private let cardTeamsPath = "teams"
    let networkApi: HTTPManagerProtocol
    init(networkApi: HTTPManagerProtocol) {
        self.networkApi = networkApi
    }
    
    func getCardMembers(completed: @escaping (AppApiResponse) -> Void) {
        networkApi.get(url: cardMemberPath, params: [:], completionBlock: completed)
    }
    
    func getCardHolders(completed: @escaping (AppApiResponse) -> Void) {
        networkApi.get(url: cardHodlersPath, params: [:], completionBlock: completed)
    }
    
    func getCardTeams(completed: @escaping (AppApiResponse) -> Void) {
        networkApi.get(url: cardTeamsPath, params: [:], completionBlock: completed)
    }
}
