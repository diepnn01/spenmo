//
//  FilterApi.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation

protocol FilterApi {
    func getCardMembers(completed: @escaping (AppApiResponse) -> Void)
    func getCardHolders(completed: @escaping (AppApiResponse) -> Void)
    func getCardTeams(completed: @escaping (AppApiResponse) -> Void)
}
