//
//  FilterStorage.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation
protocol FilterStorage {
    func getCardMembers(completed: (([CardNumber]?, AppApiError?) -> Void)?)
    func getCardHolders(completed: (([CardHolder]?, AppApiError?) -> Void)?)
    func getCardTeams(completed: (([Team]?, AppApiError?) -> Void)?)

}
