//
//  FilterStorageimpl.swift
//  SpenmoAssignment
//
//  Created by Darren Nguyen on 31/07/2022.
//

import Foundation
class FilterStorageImpl: FilterStorage {
    let filterRepository: FilterRepository!
    var cache = NSCache<NSString, AnyObject>()
    init(filterRepository: FilterRepository) {
        self.filterRepository = filterRepository
    }
    func getCardMembers(completed: (([CardNumber]?, AppApiError?) -> Void)?) {
        if let data = cache.object(forKey: "getCardMembers") as? CardNumberResponse {
            completed?(data.cards, nil)
        } else {
            filterRepository.getCardMembers { [weak self] response, error in
                if let response = response, !response.cards.isEmpty {
                    self?.cache.setObject(response, forKey: "getCardMembers")
                }
                completed?(response?.cards, error)
            }
        }
    }
    
    func getCardHolders(completed: (([CardHolder]?, AppApiError?) -> Void)?) {
        if let data = cache.object(forKey: "getCardHolders") as? CardHolderResponse {
            completed?(data.cardHolders, nil)
        } else {
            filterRepository.getCardHolders { [weak self] response, error in
                if let response = response, !response.cardHolders.isEmpty {
                    self?.cache.setObject(response, forKey: "getCardHolders")
                }
                completed?(response?.cardHolders, error)
            }
        }
    }
    
    func getCardTeams(completed: (([Team]?, AppApiError?) -> Void)?) {
        if let data = cache.object(forKey: "getCardTeams") as? CardTeamResponse {
            completed?(data.teams, nil)
        } else {
            filterRepository.getCardTeams { [weak self] response, error in
                if let response = response, !response.teams.isEmpty {
                    self?.cache.setObject(response, forKey: "getCardTeams")
                }
                completed?(response?.teams, error)
            }
        }
    }
}
