//
//  UIFont+Ext.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/30.
//

import UIKit

extension UIFont {
    
    static func avenirRegular(fontSize: Float) -> UIFont {
        guard let avenirRegular = UIFont(name: "Avenir Regular", size: CGFloat(fontSize)) else {
            return UIFont.systemFont(ofSize: CGFloat(fontSize))
        }
        return avenirRegular
    }
    
    static func avenirMedium(fontSize: Float) -> UIFont {
        guard let avenirMedium = UIFont(name: "Avenir Medium", size: CGFloat(fontSize)) else {
            return UIFont.systemFont(ofSize: CGFloat(fontSize))
        }
        return avenirMedium
    }
    
    static func avenirBlack(fontSize: Float) -> UIFont {
        guard let avenirBlack = UIFont(name: "Avenir Black", size: CGFloat(fontSize)) else {
            return UIFont.systemFont(ofSize: CGFloat(fontSize))
        }
        return avenirBlack
    }
    
    static func avenirRoman(fontSize: Float) -> UIFont {
        guard let avenirRoman = UIFont(name: "Avenir-Roman", size: CGFloat(fontSize)) else {
            return UIFont.systemFont(ofSize: CGFloat(fontSize))
        }
        return avenirRoman
    }
    
    static func publicSansBold(fontSize: Float) -> UIFont {
        guard let publicSansBold = UIFont(name: "PublicSans-Bold", size: CGFloat(fontSize)) else {
            return UIFont.systemFont(ofSize: CGFloat(fontSize))
        }
        return publicSansBold
    }
}


