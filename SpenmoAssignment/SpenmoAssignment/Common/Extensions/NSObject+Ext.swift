//
//  NSObject+Ext.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/29.
//

import Foundation

extension NSObject {
    
    var className: String {
        
        return String(describing: type(of: self))
    }
    
    class var className: String {
        
        return String(describing: self)
    }
}
