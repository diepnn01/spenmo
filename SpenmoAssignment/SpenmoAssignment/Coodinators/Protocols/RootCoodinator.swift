//
//  RootCoodinator.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/28.
//

import UIKit

protocol RootCoordinator: AnyObject {
    func start(_ navigationController: UINavigationController)
//    func selectFilterConditions()
}
