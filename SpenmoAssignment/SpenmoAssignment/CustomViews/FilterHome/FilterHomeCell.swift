//
//  FilterHomeCell.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/29.
//

import UIKit
import SnapKit
import RxSwift

final class FilterHomeCell: UITableViewCell, BaseViewType {

    var disposeBag = DisposeBag()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy private var labelTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.avenirMedium(fontSize: 14)
        return label
    }()
    
    lazy private var boxContainer: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(named: "color_border")?.cgColor
        
        return view
    }()
    
    lazy var boxFilterConditionsView: BoxFilterConditionsView = {
        let view = BoxFilterConditionsView(shouldSetup: true)
        return view
    }()
    
    lazy private var buttonArrowDown: UIButton = {
        let button = UIButton()
        button.setImage(UIImage(named: "ic_arrow_down"), for: .normal)
        return button
    }()
    
    var onSelectCondition: (() -> Void)?
    
    var filterType: FilterEnum? {
        didSet {
            labelTitle.text = filterType?.title
            boxFilterConditionsView.placeholder = filterType?.filterPlaceHolder
        }
    }
    
    func fillData(filterConditions: [Filterable]) {
        boxFilterConditionsView.items = filterConditions
    }
    
    func layout() {
        selectionStyle = .none
        [boxContainer, labelTitle].forEach(contentView.addSubview)
        [boxFilterConditionsView, buttonArrowDown].forEach(boxContainer.addSubview)
        
        labelTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(30)
        }
        
        boxContainer.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.top.equalTo(labelTitle.snp.bottom).offset(5)
            make.trailing.equalToSuperview().offset(-16)
            make.bottom.equalToSuperview().offset(-5)
        }
        
        boxFilterConditionsView.snp.makeConstraints { make in
            make.leading.top.bottom.equalToSuperview()
            make.trailing.equalTo(buttonArrowDown.snp.leading).offset(-5)
        }
        
        buttonArrowDown.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(24)
        }
    }
    
    func bind() {
        buttonArrowDown.rx.tap
            .withUnretained(self)
            .subscribe { (weakSelf, _) in
                weakSelf.onSelectCondition?()
        }.disposed(by: disposeBag)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
    
}
