//
//  BoxFilterConditionsCell.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/30.
//

import UIKit
import SnapKit
import RxSwift

final class BoxFilterConditionsCell: UICollectionViewCell, BaseViewType {
    
    var disposeBag: DisposeBag = DisposeBag()
    var onRemoveSelectedItem: (() -> Void)?
    override init(frame: CGRect) {
        super.init(frame: frame)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy private var containerView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "color_filter_condition_bg")
        view.layer.cornerRadius = 8
        return view
    }()
    
    lazy private var labelTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.avenirRegular(fontSize: 14)
        label.textColor = .white
        return label
    }()
    
    lazy var buttonRemove: UIButton = {
        let btn = UIButton()
        btn.setImage(UIImage(named: "ic_remove"), for: .normal)
        return btn
    }()
    
    
    //MARK: - properties
    var title: String? {
        didSet {
            labelTitle.text = title
        }
    }
    
    func layout() {
        [labelTitle, buttonRemove].forEach(containerView.addSubview)
        contentView.addSubview(containerView)
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        labelTitle.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(8)
            make.centerY.equalToSuperview()
            make.trailing.equalTo(buttonRemove.snp.leading)
        }
        
        buttonRemove.snp.makeConstraints { make in
            make.trailing.equalToSuperview()
            make.centerY.equalToSuperview()
            make.height.width.equalTo(20)
        }
    }
    
    func bind() {
        buttonRemove.rx.tap
            .asObservable()
            .subscribe { [weak self]_ in
                self?.onRemoveSelectedItem?()
        }.disposed(by: disposeBag)

    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}
