//
//  BoxFilterConditionsView.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/30.
//

import UIKit
import SnapKit
import RxSwift

final class BoxFilterConditionsView: UIView, BaseViewType {
    
    var disposeBag: DisposeBag = DisposeBag()
    override init(frame: CGRect) {
        super.init(frame: frame)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    var currentHeightCollectionView: CGFloat = 38
    var onChangeHeight: ((CGFloat) -> Void)?
    
    var onRemoveSelectedItem: ((Filterable) -> Void)?

    lazy private var collectionView: UICollectionView = {
        let flowLayout: UICollectionViewFlowLayout = UICollectionViewFlowLayout()
        flowLayout.scrollDirection = .horizontal
        flowLayout.estimatedItemSize = CGSize(width: 100, height: 24)
        flowLayout.itemSize = UICollectionViewFlowLayout.automaticSize
        let collectionView = UICollectionView(frame: .zero, collectionViewLayout: flowLayout)
        collectionView.backgroundColor = .clear
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.dataSource = self
        collectionView.delegate  = self
        return collectionView
    }()
    
    lazy private var labelPlaceholder: UILabel = {
        let label = UILabel()
        label.font = UIFont.avenirRegular(fontSize: 16)
        label.textColor = UIColor(named: "color_placeholder")
        return label
    }()
    
    var placeholder: String? {
        didSet {
            labelPlaceholder.text = placeholder
        }
    }
    
    var items: [Filterable] = [Filterable]() {
        didSet {
            labelPlaceholder.text = items.count == 0 ? placeholder : nil
            collectionView.reloadData()
        }
    }
    
    func layout() {
        addSubview(labelPlaceholder)
        addSubview(collectionView)
        
        labelPlaceholder.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalToSuperview().offset(-16)
            make.top.equalToSuperview().offset(12)
            make.bottom.equalToSuperview().offset(-12)
        }
        
     
            collectionView.snp.makeConstraints { make in
                make.top.leading.equalToSuperview().offset(12)
                make.trailing.bottom.equalToSuperview().offset(-12)
                make.height.equalTo(currentHeightCollectionView)
            }
            bind()
            collectionView.register(BoxFilterConditionsCell.self, forCellWithReuseIdentifier: BoxFilterConditionsCell.className)
        }
        
        func bind() {
            collectionView.rx.observe(CGSize.self, "contentSize").subscribe(onNext: { [weak self](size) in
                guard let size = size, let `self` = self else { return }
                self.updateHeight(size: size)
            }).disposed(by: disposeBag)
        }
        
        private func updateHeight(size: CGSize) {
            let heightContent = size.height
            guard heightContent != currentHeightCollectionView else {
                return
            }
            currentHeightCollectionView = heightContent > 0 ? heightContent:38
            collectionView.snp.updateConstraints { make in
                make.height.equalTo(currentHeightCollectionView)
            }
            onChangeHeight?(heightContent)
        }
}

extension BoxFilterConditionsView: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: BoxFilterConditionsCell.className, for: indexPath) as? BoxFilterConditionsCell else {
            return UICollectionViewCell()
        }
        let item = items[indexPath.row]
        cell.title = item.name
        cell.bind()
        cell.onRemoveSelectedItem = { [weak self] in
            self?.onRemoveSelectedItem?(item)
        }
        return cell
    }
}

extension BoxFilterConditionsView: UICollectionViewDelegate {
    
}

extension BoxFilterConditionsView: UICollectionViewDelegateFlowLayout {
        
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 2
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 5
    }
}

