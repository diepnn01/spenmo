//
//  SearchBoxView.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/31.
//

import Foundation
import UIKit
import RxSwift

final class SearchBoxView: UIView, BaseViewType {
    
    var disposeBag: DisposeBag = DisposeBag()
    
    var containerView: UIView = {
        let view = UIView()
        view.layer.cornerRadius = 4
        view.layer.borderWidth = 1
        view.layer.borderColor = UIColor(named: "color_border")?.cgColor
        return view
    }()
    
    lazy private var iconSearch: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_search")
        return imageView
    }()
    
    lazy var textFieldSearch: UITextField = {
        let txtField = UITextField()
        txtField.translatesAutoresizingMaskIntoConstraints = false
        txtField.delegate = self

        return txtField
    }()
    
    var placeholder: String? {
        didSet {
            textFieldSearch.placeholder = placeholder
        }
    }
    
    func layout() {
        addSubview(containerView)
        
        [iconSearch, textFieldSearch].forEach(containerView.addSubview)
        containerView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        
        iconSearch.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(20)
        }
        
        textFieldSearch.snp.makeConstraints { make in
            make.leading.equalTo(iconSearch.snp.trailing).offset(5)
            make.centerY.equalToSuperview()
            make.trailing.equalToSuperview().offset(-5)
        }
    }
    
    func bind() {
        //
    }
}

extension SearchBoxView: UITextFieldDelegate {
    
//    func textFieldDidBeginEditing(_ textField: UITextField) {
//        print("Diepnn")
//        textFieldSearch.becomeFirstResponder()
//    }
}
