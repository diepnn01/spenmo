//
//  AvatarView.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/31.
//

import UIKit
import SnapKit
import RxSwift


final class AvatarView: UIView, BaseViewType {
    
    var disposeBag: DisposeBag = DisposeBag()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy private var  circleView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(named: "color_avatar_bg")
        view.layer.cornerRadius = 20
        view.clipsToBounds = true
        return view
        
    }()
    
    lazy private var labelAvatar: UILabel = {
        let label = UILabel()
        label.textAlignment = .center
        label.textColor = UIColor(named: "color_avatar_title")
        label.font = UIFont.publicSansBold(fontSize: 16)
        return label
    }()
    
    var character: String? {
        didSet {
            labelAvatar.text = character?.uppercased()
        }
    }
    
    func layout() {
        [circleView, labelAvatar].forEach(addSubview)
        circleView.snp.makeConstraints { make in
            make.edges.equalToSuperview()
        }
        labelAvatar.snp.makeConstraints { make in
            make.center.equalToSuperview()
        }
    }
    
    func bind() {
        //
    }
}
