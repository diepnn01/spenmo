//
//  FilterCommonCell.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/31.
//

import UIKit
import RxSwift
import SnapKit

final class FilterCommonCell: UITableViewCell, BaseViewType {

    var disposeBag: DisposeBag = DisposeBag()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy private var labelName: UILabel =  {
       let label = UILabel()
        label.font = UIFont.avenirMedium(fontSize: 16)
        label.textColor = .black
        return label
    }()
    
    lazy private var selectedCheckBox: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_check")
        return imageView
    }()
    
    var name: String? {
        didSet {
            labelName.text = name
        }
    }
    
    var selectedItem: Bool = false {
        didSet {
            selectedCheckBox.isHidden = !selectedItem
        }
    }
    
    
    func layout() {
        selectionStyle = .none
        contentView.addSubview(labelName)
        contentView.addSubview(selectedCheckBox)
        
        labelName.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalTo(selectedCheckBox.snp.leading).offset(-8)
            make.centerY.equalToSuperview()
        }
        
        selectedCheckBox.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(18)
        }
        
    }
    
    func bind() {
        //
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}
