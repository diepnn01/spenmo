//
//  FilterCardHolderCell.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/31.
//

import UIKit
import SnapKit
import RxSwift

final class FilterCardHolderCell: UITableViewCell, BaseViewType {

    var disposeBag: DisposeBag = DisposeBag()
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    lazy private var avatarView: AvatarView = {
        let view = AvatarView(shouldSetup: true)
        return view
    }()
    
    lazy private var labelTitle: UILabel = {
        let label = UILabel()
        label.font = UIFont.avenirMedium(fontSize: 16)
        label.textColor = .black
        return label
    }()
    
    lazy private var selectedCheckBox: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_check")
        return imageView
    }()
    
    var cardHolder: Filterable? {
        didSet {
            guard let card = cardHolder else { return }
            
            labelTitle.text = card.name
            let index = card.name.index(card.name.startIndex, offsetBy: 0)
            let firstCharacter = String(card.name[index])    // "S"
            avatarView.character = firstCharacter
        }
    }
    
    var isSelectedItem: Bool = false {
        didSet {
            selectedCheckBox.isHidden = !isSelectedItem
        }
    }
    
    func layout() {
        selectionStyle = .none
        [avatarView, labelTitle, selectedCheckBox].forEach(contentView.addSubview)
        avatarView.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(40)
        }
        
        labelTitle.snp.makeConstraints { make in
            make.leading.equalTo(avatarView.snp.trailing).offset(8)
            make.centerY.equalToSuperview()
            make.trailing.equalTo(selectedCheckBox.snp.leading).offset(-8)
        }
        
        selectedCheckBox.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(18)
        }
        
    }
    
    func bind() {
        //
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }

}
