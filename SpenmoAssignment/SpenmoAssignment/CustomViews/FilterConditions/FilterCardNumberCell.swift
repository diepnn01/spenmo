//
//  FilterCardNumberCell.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/31.
//

import UIKit
import RxSwift
import SnapKit

final class FilterCardNumberCell: UITableViewCell, BaseViewType {

    var disposeBag: DisposeBag = DisposeBag()
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        layout()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    //MARK: UI Components
    lazy private var labelCardName: UILabel =  {
       let label = UILabel()
        label.font = UIFont.avenirMedium(fontSize: 16)
        label.textColor = .black
        return label
    }()
    
    lazy private var selectedCheckBox: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_check")
        return imageView
    }()
    
    var cardNumber: Filterable? {
        didSet {
            guard let card = cardNumber else {
                return
            }
            let cardNameAttributes: [NSAttributedString.Key: Any] = [.foregroundColor: UIColor.black, .font: UIFont.avenirMedium(fontSize: 16)]
            let cardNumberAttributes = [NSAttributedString.Key.foregroundColor: UIColor(named: "color_card_number") ?? .gray, .font: UIFont.avenirRoman(fontSize: 16)]

            let cardNameString = NSMutableAttributedString(string: card.name, attributes: cardNameAttributes)
            let cardNumberString = NSAttributedString(string: card.subTitle, attributes: cardNumberAttributes)
            
            cardNameString.append(cardNumberString)
            labelCardName.attributedText = cardNameString
        }
    }
    
    var isSelectedItem: Bool = false {
        didSet {
            selectedCheckBox.isHidden = !isSelectedItem
        }
    }
    
    func layout() {
        selectionStyle = .none
        contentView.addSubview(labelCardName)
        contentView.addSubview(selectedCheckBox)
        
        labelCardName.snp.makeConstraints { make in
            make.leading.equalToSuperview().offset(16)
            make.trailing.equalTo(selectedCheckBox.snp.leading).offset(-8)
            make.centerY.equalToSuperview()
        }
        
        selectedCheckBox.snp.makeConstraints { make in
            make.trailing.equalToSuperview().offset(-16)
            make.centerY.equalToSuperview()
            make.height.width.equalTo(18)
        }
        
    }
    
    func bind() {
        //
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        disposeBag = DisposeBag()
    }
}
