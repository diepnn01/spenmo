//
//  Factory.swift
//  SpenmoAssignment
//
//  Created by Nguyen Ngoc Diep on 2022/07/28.
//

import Foundation

protocol Factory {
    func makeFilterHomeViewController(coordinator: ProjectCoordinator) -> FilterHomeController
    func makeFilterHomeViewModel(coordinator: RootCoordinator) -> FilterHomeViewModel
}

class DependencyFactory: Factory {
    func makeFilterHomeViewController(coordinator: ProjectCoordinator) -> FilterHomeController {
        let viewModel = makeFilterHomeViewModel(coordinator: coordinator)
        let filterHomeController = FilterHomeController(coordinator: coordinator, viewModel: viewModel)
        return filterHomeController
    }
    
    func makeFilterHomeViewModel(coordinator: RootCoordinator) -> FilterHomeViewModel {
        return FilterHomeViewModel(coordinator: coordinator, filterStorage: SPAppDependency.filterStorage)
    }
    
    func makeInitialCoordinator() -> ProjectCoordinator {
        let coordinator = ProjectCoordinator(factory: self)
        return coordinator
    }
    
}
