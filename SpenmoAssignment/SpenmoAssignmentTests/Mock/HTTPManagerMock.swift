//
//  HTTPManagerMock.swift
//  SpenmoAssignmentTests
//
//  Created by Nguyen Ngoc Diep on 2022/08/01.
//

import Foundation
@testable import SpenmoAssignment

class HTTPManagerMock: HTTPManagerProtocol {
    
    var configuration: AppConfiguration!
    init(configuration: AppConfiguration) {
        self.configuration = configuration
    }
    
    func get(url: String, params: [String : String], completionBlock: @escaping (AppApiResponse) -> Void) {
        let data = Data("The Test Data".utf8)
        completionBlock(AppApiResponse(data: data, error: nil, message: nil))
    }
}
