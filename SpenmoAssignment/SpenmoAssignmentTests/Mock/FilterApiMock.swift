//
//  FilterApiMock.swift
//  SpenmoAssignmentTests
//
//  Created by Nguyen Ngoc Diep on 2022/08/01.
//

import Foundation
@testable import SpenmoAssignment

class FilterApiMock: FilterApi {
    private let cardMemberPath = "cards"
    private let cardHodlersPath = "cardholders"
    private let cardTeamsPath = "teams"
    let networkApi: HTTPManagerProtocol
    init(networkApi: HTTPManagerProtocol) {
        self.networkApi = networkApi
    }
    
    func getCardMembers(completed: @escaping (AppApiResponse) -> Void) {
        let data = self.readLocalFile(forName: "cardnumbers")
        completed(AppApiResponse(data: data, error: nil, message: nil))
    }
    
    func getCardHolders(completed: @escaping (AppApiResponse) -> Void) {
        let data = self.readLocalFile(forName: "cardholders")
        completed(AppApiResponse(data: data, error: nil, message: nil))
    }
    
    func getCardTeams(completed: @escaping (AppApiResponse) -> Void) {
        let data = self.readLocalFile(forName: "teams")
        completed(AppApiResponse(data: data, error: nil, message: nil))
    }
    
    private func readLocalFile(forName name: String) -> Data? {
        do {
            if let bundlePath = Bundle.main.path(forResource: name,
                                                 ofType: "json"),
                let jsonData = try String(contentsOfFile: bundlePath).data(using: .utf8) {
                return jsonData
            }
        } catch {
            print(error)
        }
        
        return nil
    }
}


