//
//  FactoryMock.swift
//  SpenmoAssignmentTests
//
//  Created by Nguyen Ngoc Diep on 2022/08/01.
//

import XCTest
@testable import SpenmoAssignment

class FactoryMock: Factory {
    
    func makeFilterHomeViewController(coordinator: ProjectCoordinator) -> FilterHomeController {
        let viewModel = makeFilterHomeViewModel(coordinator: coordinator)
        let initialViewController = FilterHomeController(coordinator: coordinator, viewModel: viewModel)
        return initialViewController
    }
    
    func makeFilterHomeViewModel(coordinator: RootCoordinator) -> FilterHomeViewModel {
        return FilterHomeViewModel(coordinator: coordinator, filterStorage: SPAppDependency.filterStorage)
    }
    
    func makeInitialCoordinator() -> ProjectCoordinator {
        let coordinator = ProjectCoordinator(factory: self)
        return coordinator
    }
    
    
    
}
