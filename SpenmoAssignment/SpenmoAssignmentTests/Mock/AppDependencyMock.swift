//
//  AppDependencyMock.swift
//  SpenmoAssignmentTests
//
//  Created by Nguyen Ngoc Diep on 2022/08/01.
//

import Foundation
@testable import SpenmoAssignment

class AppDependencyMock {
    static let shared: AppDependencyMock = AppDependencyMock()
    lazy var configation: AppConfiguration = {
       return AppConfiguration()
    }()
    
    lazy var httpManager: HTTPManagerProtocol = {
       return HTTPManagerMock(configuration: configation)
    }()
    
    lazy var filterApi: FilterApi = {
       return FilterApiMock(networkApi: httpManager)
    }()
    
    lazy var filterRepository: FilterRepository = {
       return FilterRepositoryImpl(filterApi: filterApi)
    }()
    
    lazy var filterStorage: FilterStorage = {
       return FilterStorageImpl(filterRepository: filterRepository)
    }()
}
