//
//  FilterHomeViewModelTest.swift
//  SpenmoAssignmentTests
//
//  Created by Nguyen Ngoc Diep on 2022/08/01.
//

import XCTest
@testable import SpenmoAssignment

class FilterHomeViewModelTest: XCTestCase {

    
    var viewModel: FilterHomeViewModel?
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        let factory = FactoryMock()
        let coordinator = ProjectCoordinator(factory: factory)
        viewModel = factory.makeFilterHomeViewModel(coordinator: coordinator)
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testFilterHomeViewModel_WhenRemoveSelectedFilterCondition_ShouldReturnTrue() {
        
        viewModel?.onSetSelectedCardNumber(cards: [CardNumber(cardName: "Watson Personal", cardId: "ee445c28-abcb-145c-8ca8-02ab450d4310", cardNumber: "1346"),
                                                   CardNumber(cardName: "Warren Virtual", cardId: "9ed0ed98-377a-469f-a7ec-9321e5ccaf63", cardNumber: "2104")])
        viewModel?.remove(item: CardNumber(cardName: "Watson Personal", cardId: "ee445c28-abcb-145c-8ca8-02ab450d4310", cardNumber: "1346"), with: .cardNumber)
        
        XCTAssertTrue(viewModel?.cardNumbersSelected.count == 1, "The Number of searchItems should equal 1, but it hasn't returned 1")
    }

}
