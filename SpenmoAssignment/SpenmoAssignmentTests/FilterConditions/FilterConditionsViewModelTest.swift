//
//  FilterConditionsViewModelTest.swift
//  SpenmoAssignmentTests
//
//  Created by Nguyen Ngoc Diep on 2022/08/01.
//

import XCTest
@testable import SpenmoAssignment

class FilterConditionsViewModelTest: XCTestCase {

    var viewModel: FilterConditionsViewModel?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
    }

    func testFilterCondtionViewModel_WhenGetListCardNumbers_ShouldReturnTrue() {
        let type = FilterEnum.cardNumber
        let storage = AppDependencyMock.shared.filterStorage
        viewModel = FilterConditionsViewModel(filterType: type, filterStorage: storage)
        let expectation = XCTestExpectation(description: #function)
        viewModel?.didGetData = { items in
            expectation.fulfill()
            XCTAssertTrue(items.count > 0, "Cannot get card number list")
        }
        viewModel?.getCardNumbers()
        wait(for: [expectation], timeout: 3.0)
    }
    
    func testFilterCondtionViewModel_WhenGetListCardHolder_ShouldReturnTrue() {
        let type = FilterEnum.cardHolder
        let storage = AppDependencyMock.shared.filterStorage
        viewModel = FilterConditionsViewModel(filterType: type, filterStorage: storage)
        let expectation = XCTestExpectation(description: #function)
        viewModel?.didGetData = { items in
            expectation.fulfill()
            XCTAssertTrue(items.count > 0, "Cannot get card holder list")
        }
        viewModel?.getCardHolder()
        wait(for: [expectation], timeout: 3.0)
    }
    
    
    func testFilterCondtionViewModel_WhenGetListTeam_ShouldReturnTrue() {
        let type = FilterEnum.team
        let storage = AppDependencyMock.shared.filterStorage
        viewModel = FilterConditionsViewModel(filterType: type, filterStorage: storage)
        let expectation = XCTestExpectation(description: #function)
        viewModel?.didGetData = { items in
            expectation.fulfill()
            XCTAssertTrue(items.count > 0, "Cannot get card holder list")
        }
        viewModel?.getCardTeam()
        wait(for: [expectation], timeout: 3.0)
    }

    
    func testFilterConditionViewModel_WhenSelectNewCondition_ShouldReturnFalse() {
        
        let type = FilterEnum.cardNumber
        let storage = AppDependencyMock.shared.filterStorage
        viewModel = FilterConditionsViewModel(filterType: type, filterStorage: storage)
        guard let viewModel = viewModel else {
            return
        }

        
        let newSelectedCardNumber = CardNumber(cardName: "watson Personal",
                                               cardId: "ee445c28-abcb-145c-8ca8-02ab450d4310",
                                               cardNumber: "1234")
        
        let selectedCardNumbers = [CardNumber(cardName: "Warren Virtual",
                                              cardId: "9ed0ed98-377a-469f-a7ec-9321e5ccaf63",
                                              cardNumber: "2104"),
                                  CardNumber(cardName: "LinkedIn Ads",
                                             cardId: "6334dc32-83da-4593-b2de-0769d06216d",
                                             cardNumber: "6019"),
                                  CardNumber(cardName: "Marketing Expenses",
                                             cardId: "07103200-ee2a-11ec-bcf1-0a58a9feac02",
                                             cardNumber: "0443")]
        
        viewModel.selectedItems = selectedCardNumbers

        let isExisted = viewModel.checkSelectedItem(item: newSelectedCardNumber)
        XCTAssertFalse(isExisted, "The Existed should return FALSE, it has returned TRUE")
    }
    
    func testFilterConditionViewModel_WhenSelecteNewCondition_ShouldReturnTrue() {
        
        let type = FilterEnum.cardNumber
        let storage = AppDependencyMock.shared.filterStorage
        viewModel = FilterConditionsViewModel(filterType: type, filterStorage: storage)
        guard let viewModel = viewModel else {
            return
        }

        
        let newSelectedCardNumber = CardNumber(cardName: "Watson Personal", cardId: "ee445c28-abcb-145c-8ca8-02ab450d4310", cardNumber: "1346")
        
        let selectedCardNumbers = [CardNumber(cardName: "Watson Personal",
                                              cardId: "ee445c28-abcb-145c-8ca8-02ab450d4310",
                                              cardNumber: "1346"),
                                  CardNumber(cardName: "LinkedIn Ads",
                                             cardId: "6334dc32-83da-4593-b2de-0769d06216d",
                                             cardNumber: "6019"),
                                  CardNumber(cardName: "Marketing Expenses",
                                             cardId: "07103200-ee2a-11ec-bcf1-0a58a9feac02",
                                             cardNumber: "0443")]
        
        viewModel.selectedItems = selectedCardNumbers

        let isExisted = viewModel.checkSelectedItem(item: newSelectedCardNumber)
        XCTAssertTrue(isExisted, "The Existed should return TRUE, it has returned FALSE")
    }
    
    
    func testFilterConditionViewModel_WhenSearchConditions_ShouldReturnTrue() {
        
        let type = FilterEnum.cardNumber
        let storage = AppDependencyMock.shared.filterStorage
        viewModel = FilterConditionsViewModel(filterType: type, filterStorage: storage)
        guard let viewModel = viewModel else {
            return
        }

        let items = [CardNumber(cardName: "Watson Personal", cardId: "ee445c28-abcb-145c-8ca8-02ab450d4310", cardNumber: "1346"),
                      CardNumber(cardName: "Warren Virtual", cardId: "9ed0ed98-377a-469f-a7ec-9321e5ccaf63", cardNumber: "2104"),
                      CardNumber(cardName: "LinkedIn Ads", cardId: "6334dc32-83da-4593-b2de-0769d06216d", cardNumber: "6019"),
                      CardNumber(cardName: "Marketing Expenses", cardId: "07103200-ee2a-11ec-bcf1-0a58a9feac02", cardNumber: "0443"),
                      CardNumber(cardName: "Engineering", cardId: "04c4bd2c-ee2a-11ec-9a31-0a58a9feac02", cardNumber: "9641"),
                      CardNumber(cardName: "HR Expenses", cardId: "d585b4fa-4f36-4a89-87e7-9a16613c73f", cardNumber: "2183"),
                      CardNumber(cardName: "CEO card", cardId: "07103200-ee2a-11ec-bcf1-0a58a9feac02", cardNumber: "1408"),
                      CardNumber(cardName: "Finance Department", cardId: "04c4bd2c-ee2a-11ec-9a31-0a58a9feac02", cardNumber: "7810"),
                      CardNumber(cardName: "Software Subscriptions", cardId: "d585b4fa-4f36-4a89-87e7-9a16613c73f", cardNumber: "4843")]
        
        viewModel.items = items
        
        viewModel.search(with: "w")
        XCTAssertTrue(viewModel.searchItems.count == 3, "The Number of searchItems should equal 3, but it hasn't returned 3")
    }
    
    func testFilterConditionViewModel_WhenSearchConditions_ShouldReturnFalse() {
        
        let type = FilterEnum.cardNumber
        let storage = AppDependencyMock.shared.filterStorage
        viewModel = FilterConditionsViewModel(filterType: type, filterStorage: storage)
        guard let viewModel = viewModel else {
            return
        }

        let items = [CardNumber(cardName: "Watson Personal", cardId: "ee445c28-abcb-145c-8ca8-02ab450d4310", cardNumber: "1346"),
                      CardNumber(cardName: "Warren Virtual", cardId: "9ed0ed98-377a-469f-a7ec-9321e5ccaf63", cardNumber: "2104"),
                      CardNumber(cardName: "LinkedIn Ads", cardId: "6334dc32-83da-4593-b2de-0769d06216d", cardNumber: "6019"),
                      CardNumber(cardName: "Marketing Expenses", cardId: "07103200-ee2a-11ec-bcf1-0a58a9feac02", cardNumber: "0443"),
                      CardNumber(cardName: "Engineering", cardId: "04c4bd2c-ee2a-11ec-9a31-0a58a9feac02", cardNumber: "9641"),
                      CardNumber(cardName: "HR Expenses", cardId: "d585b4fa-4f36-4a89-87e7-9a16613c73f", cardNumber: "2183"),
                      CardNumber(cardName: "CEO card", cardId: "07103200-ee2a-11ec-bcf1-0a58a9feac02", cardNumber: "1408"),
                      CardNumber(cardName: "Finance Department", cardId: "04c4bd2c-ee2a-11ec-9a31-0a58a9feac02", cardNumber: "7810"),
                      CardNumber(cardName: "Software Subscriptions", cardId: "d585b4fa-4f36-4a89-87e7-9a16613c73f", cardNumber: "4843")]
        
        viewModel.items = items
        
        viewModel.search(with: "Diep")
        XCTAssertFalse(viewModel.searchItems.count > 0, "The Number of searchItems should equal 0, but it hasn't returned the value is different 0")
    }
}
