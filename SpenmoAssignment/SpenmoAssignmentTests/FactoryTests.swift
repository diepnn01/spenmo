//
//  FactoryTests.swift
//  SpenmoAssignmentTests
//
//  Created by Nguyen Ngoc Diep on 2022/08/01.
//

import XCTest
@testable import SpenmoAssignment

class FactoryTests: XCTestCase {

    var dependencyFactory: DependencyFactory?
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
        dependencyFactory = DependencyFactory()
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

}
